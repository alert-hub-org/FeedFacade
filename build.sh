#!/bin/bash
export SDKMAN_DIR="/home/ibbo/.sdkman"
[[ -s "/home/ibbo/.sdkman/bin/sdkman-init.sh" ]] && source "/home/ibbo/.sdkman/bin/sdkman-init.sh"

export FF_VER=`grep appVersion ./service/gradle.properties | cut -f2 -d=`

echo Release FeedFacade $FF_VER

sdk use grails 4.1.3
sdk use java 11.0.22-tem
cd service
./gradlew clean
./gradlew build
cp build/libs/feedFacade-*.jar ../docker/feedFacade.jar
cd ../docker
docker login

if [[ "$FF_VER" == *-SNAPSHOT ]]
then
  echo  SNAPSHOT release - only tagging :v$FF_VER and :latest
  docker build -t semweb/caphub_feedfacade:v$FF_VER -t semweb/caphub_feedfacade:latest .
  docker push semweb/caphub_feedfacade:v$FF_VER
  docker push semweb/caphub_feedfacade:latest
else
  echo  Standard Release
  docker build -t semweb/caphub_feedfacade:v$FF_VER -t semweb/caphub_feedfacade:v3.0 -t semweb/caphub_feedfacade:v3 -t semweb/caphub_feedfacade:latest .
  docker push semweb/caphub_feedfacade:v$FF_VER
  docker push semweb/caphub_feedfacade:v3.0
  docker push semweb/caphub_feedfacade:v3
  docker push semweb/caphub_feedfacade:latest
fi

echo Completed release of FeedFacade $FF_VER
echo to deploy
echo ssh cap
echo docker service update --image semweb/caphub_feedfacade:v$FF_VER fah_feedFacade
