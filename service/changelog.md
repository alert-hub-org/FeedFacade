# Changelog

## Version 3.2.1

### Changes
* [Chore]
	* updated readme
	* build - Take project version from conventional commits
	* build - Reconfigure build file for conventional commits
* [Wip]
	* post to slack when issues happen

## Version 3.2.0

### Changes
* [Chore]
	* build - Add conventional commits plugin
	* logging - More instrumentation around blocked feeds
	* release - version to 3.2.0
