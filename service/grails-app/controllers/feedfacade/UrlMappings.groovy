package feedfacade

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller:'home', action:'index')
        "/status"(controller:'home', action:'status')
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
