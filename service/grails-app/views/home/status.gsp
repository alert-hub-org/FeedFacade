<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
  <h1>Feed enabled status</h1>
  <table class="table table-striped">
  <tr><th>Enabled</th><th>Count</th></tr>
  <g:each in="${enabledSummary}" var="es">
    <tr>
      <td> ${es.status} </td>
      <td> ${es.count} </td>
    </tr>
  </g:each>
  </table>

  <h1>Feed Checker Status</h1>
  <table class="table table-striped">
  <g:each in="${checkerStatusSummary}" var="es">
    <tr>
      <td> ${es.status} </td>
      <td> ${es.count} </td>
    </tr>
  </g:each>
  </table>

  <h1>Public Feed status</h1>
  <table class="table table-striped">
  <g:each in="${feedStatusSummary}" var="es">
    <tr>
      <td> ${es.status} </td>
      <td> ${es.count} </td>
    </tr>
  </g:each>
  </table>


  <h1>Active Checks</h1>
  <table class="table table-striped">
  <g:each in="${activeChecks}" var="es">
    <tr>
      <td>${es.value.id}</td>
      <td>${es.value.uriname}</td>
      <td>${es.value.url}</td>
      <td>${es.value.start_time}</td>
    </tr>
  </g:each>
  </table>

  <h1>Last 100 log messages</h1>
  <table class="table table-striped">
  <g:each in="${lastLog}" var="ll">
    <tr>
      <td>${ll.timestamp}</td>
      <td>${ll.message}</td>
    </tr>
  </g:each>
  </table>
</body>
</html>
